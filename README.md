# CODE AND SMALL PROGRAMS FOR STATISTICAL DATA ANALYSIS IN NUCLEAR AND SUBNUCLEAR PHYSICS

This repository contains code snippets and small programs written in C++ using the ROOT framework.
To build the code in each folder:

```shell
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build . -j4 --config Release
```

## CURRENTLY WORKING CODES

As of the last commit the following codes are correctly working:

- Metropolis-Hastings Algorithm
