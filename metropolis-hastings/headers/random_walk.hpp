#ifndef RANDOM_WALK_HXX
#define RANDOM_WALK_HXX
#include <vector>
#include <random>
#include <type_traits>
#include <algorithm>
#include <iostream>
template <typename FP>
class rwalk
{
public:
    rwalk() = default;
    template <typename TARGET, typename GEN = std::mt19937, typename DISTR = std::normal_distribution<FP>>
    void generate_sample(int size, TARGET target, std::vector<FP> &x0, GEN &gen = gen0, DISTR &distr = d);
    std::vector<std::vector<FP>> const & get_sample() const;

private:
    static_assert(std::is_floating_point_v<FP>);
     static std::uniform_real_distribution<FP> u;
     static std::mt19937 gen0;
     static std::normal_distribution<FP> d;
    std::vector<std::vector<FP>> sample;
};

template <typename FP>
template <typename TARGET, typename GEN, typename DISTR>
inline void rwalk<FP>::generate_sample(int size, TARGET target, std::vector<FP> &x0, GEN &gen, DISTR &distr)
{
    //static_assert(std::is_invocable_v<TARGET>);
    int vsize = x0.size();
    std::vector<FP> dx(vsize);
    std::vector<FP> x(vsize);

    while (int(sample.size())!=size)
    {
        for (auto &it : dx)
        {
            it = distr(gen);
        }
        std::transform(x0.begin(),x0.end(),dx.begin(),x.begin(),[](double x1, double x2){return x1+x2;});
        FP alpha = target(x) / target(x0);
        if (u(gen) < alpha)
        {
            x0 = x;
            sample.push_back(x0);
            dx = std::vector<FP>(vsize);
        }
        else
            for (auto &it : dx)
            {
                it = 0.;
            }
    }
}
template <typename FP>
std::vector<std::vector<FP>> const & rwalk<FP>::get_sample() const
{
    return sample;
}
template<typename FP>
std::uniform_real_distribution<FP> rwalk<FP>::u(0,1);
template<typename FP>
std::mt19937 rwalk<FP>::gen0(std::random_device{}());
template<typename FP>
std::normal_distribution<FP> rwalk<FP>::d(0,1);
#endif