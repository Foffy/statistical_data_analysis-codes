#include "random_walk.hpp"
#include <vector>
#include <random>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TF2.h>
#include <TGraph2D.h>
#include <TGraph.h>
#define MEAN 0.
#define SIGMA 1.
#define DIMENSION 2
#define SAMPLE_SIZE 1E6
#define NORM 1/sqrt(2*M_PI)
struct target
{
    public:
    double operator()(std::vector<double> const& v)
    {
        double f = 1.;
        double g = 1.;
        double h = 1.;
        for(auto const &it: v)
        {
            f *= exp(-0.5*it*it) * NORM;
            g *= exp(-0.5*(it-2)*(it-2)/4) * NORM/2;
            h *= exp(-0.5*(it+2)*(it+2)/25) * NORM/5;
        }
        return f+g+h;
    }
};

int main()
{
    std::mt19937 gen(std::random_device{}());
    std::normal_distribution g(MEAN, SIGMA);
    std::vector<double> x(DIMENSION);
    target t;
    rwalk<double> chain;
    chain.generate_sample(1e5,t,x);
    
      ////////////////////////////
      //      Setting Style     //
      ////////////////////////////

      gStyle->SetOptTitle(1);
      gStyle->SetOptStat(10);
      gStyle->SetOptFit(1110);
      gStyle->SetStatY(0.9);
      gStyle->SetStatX(0.9);
      gStyle->SetStatW(0.18);
      gStyle->SetStatH(0.18);

      Double_t w = 4000;
      Double_t h = 4000;
      TCanvas *canvas = new TCanvas();
      canvas->SetWindowSize(w, h );


    TH2F *h2d = new TH2F("MChain", "Markov Chain 2D Gaus",25,-5,5,25,-5,5);
    std::vector<std::vector<double>> const &sample = chain.get_sample();
    double *xs = new double[sample.size()]();
    double *ys = new double[sample.size()]();
    for (auto const &it: sample)
    {
        h2d->Fill(it[0],it[1]);
        //std::cout << it[0] << '\n';
    }
    h2d->Draw("LEGO4");
    canvas->Print("rwalk.pdf");
    canvas->Clear();
    delete h2d;
    for(int i = 0; i != int(sample.size()); ++i)
    {
        xs[i] = sample[i][0];
        ys[i] = sample[i][1];
    }
    TGraph g2d(sample.size(),xs,ys);
    g2d.Draw("AL*");
    canvas->Print("random_walk.pdf");
    canvas->Print("random_walk.png");
    delete[] xs;
    delete[] ys;
    delete canvas;
}
